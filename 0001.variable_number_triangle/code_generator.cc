#include<fast_io.h>
#include<fast_io_device.h>

int main()
{
	constexpr std::size_t rounds{80};
	constexpr std::size_t sub{13};
	{
	fast_io::obuf_file obf("fast_io.cc");
	print(obf,R"abc(#include<fast_io.h>

int main()
{
	std::size_t a[)abc",sub,R"abc(];
	for(std::size_t i{};i!=)abc",sub,R"abc(;++i)
		scan(a[i]);
)abc");
	for(std::size_t i{};i!=rounds;++i)
	{
		for(std::size_t j{};j!=sub;++j)
		{
			print(obf,"\tprint(");
			for(std::size_t k{};k<j;++k)
				print(obf,"a[",k,"],");
			print(obf,"a[",j,"]);\n");
		}
	}
print(obf,"}\n");
	}
	{
	fast_io::obuf_file obf("fast_io_out_buf.cc");
	print(obf,R"abc(#include<fast_io.h>

int main()
{
	std::size_t a[)abc",sub,R"abc(];
	auto ob{fast_io::out_buf()};
)abc");
	for(std::size_t i{};i!=rounds;++i)
	{
		for(std::size_t j{};j!=sub;++j)
		{
			print(obf,"\tprint(ob,");
			for(std::size_t k{};k<j;++k)
				print(obf,"a[",k,"],");
			print(obf,"a[",j,"]);\n");
		}
	}
print(obf,"}\n");
	}
	{
	fast_io::obuf_file obf("fmt.cc");
	print(obf,R"abc(#include<fmt/format.h>
#include<stdio.h>

int main()
{
	std::size_t a[)abc",sub,R"abc(];
	for(std::size_t i{};i!=)abc",sub,R"abc(;++i)
		scanf("%zu",a+i);
)abc");
	for(std::size_t i{};i!=rounds;++i)
	{
		for(std::size_t j{};j!=sub;++j)
		{
			print(obf,"\tfmt::print(\"");
			for(std::size_t k{};k<=j;++k)
				print(obf,"{}");
			print(obf,"\",");
			for(std::size_t k{};k<j;++k)
				print(obf,"a[",k,"],");
			print(obf,"a[",j,"]);\n");
		}
	}
print(obf,"}\n");
	}
	{
	fast_io::obuf_file obf("stdio.cc");
	print(obf,R"abc(#include<stdio.h>
	#include<cstddef>

int main()
{
	std::size_t a[)abc",sub,R"abc(];
	for(std::size_t i{};i!=)abc",sub,R"abc(;++i)
		scanf("%zu",a+i);
)abc");
	for(std::size_t i{};i!=rounds;++i)
	{
		for(std::size_t j{};j!=sub;++j)
		{
			print(obf,"\tprintf(\"");
			for(std::size_t k{};k<=j;++k)
				print(obf,"%zu");
			print(obf,"\",");
			for(std::size_t k{};k<j;++k)
				print(obf,"a[",k,"],");
			print(obf,"a[",j,"]);\n");
		}
	}
print(obf,"}\n");
	}
	{
	fast_io::obuf_file obf("iostream.cc");
	print(obf,R"abc(#include<iostream>
	#include<cstddef>

int main()
{
	std::size_t a[)abc",sub,R"abc(];
	for(std::size_t i{};i!=)abc",sub,R"abc(;++i)
		std::cin>>a[i];
)abc");
	for(std::size_t i{};i!=rounds;++i)
	{
		for(std::size_t j{};j!=sub;++j)
		{
			print(obf,"\tstd::cout<<");
			for(std::size_t k{};k<j;++k)
				print(obf,"a[",k,"]<<");
			print(obf,"a[",j,"];\n");
		}
	}
print(obf,"}\n");
	}
}